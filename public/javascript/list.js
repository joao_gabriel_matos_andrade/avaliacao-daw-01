window.onload = filtro("recarregar");

function filtro(quando) {
  if (quando == "recarregar") {
    $.ajax({
      url: "/search",
      type: "GET",
      data: {
        select: " ",
      },
    })
      .done(function (aluno) {
        let alunosRetornados = "";
        for (let i in aluno) {
          alunosRetornados += "<tr>";
          if (aluno[i].nome.includes("João Gabriel")) {
            alunosRetornados += "<td>" + aluno[i].nome.bold() + "</td>";
            alunosRetornados += "<td>" + aluno[i].email.bold() + "</td>";
          } else {
            alunosRetornados += "<td>" + aluno[i].nome + "</td>";
            alunosRetornados += "<td>" + aluno[i].email + "</td>";
          }
          alunosRetornados += "</tr>";
        }
        $("table tbody").html(alunosRetornados);
      })
      .fail(function (jqXHR, textStatus, msg) {
        alert(msg);
      });
  } else {
    $.ajax({
      url: "/search",
      type: "GET",
      data: {
        select: $("input[name='query']").val(),
      },
    })
      .done(function (aluno) {
        let alunosRetornados = "";
        for (let i in aluno) {
          alunosRetornados += "<tr>";
          if (aluno[i].nome.includes("João Gabriel")) {
            alunosRetornados += "<td>" + aluno[i].nome.bold() + "</td>";
            alunosRetornados += "<td>" + aluno[i].email.bold() + "</td>";
          } else {
            alunosRetornados += "<td>" + aluno[i].nome + "</td>";
            alunosRetornados += "<td>" + aluno[i].email + "</td>";
          }
          alunosRetornados += "</tr>";
        }
        $("table tbody").html(alunosRetornados);
      })
      .fail(function (jqXHR, textStatus, msg) {
        alert(msg);
      });
  }
}